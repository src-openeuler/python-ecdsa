%global _empty_manifest_terminate_build 0
Name:		python-ecdsa
Version:	0.19.0
Release:	1
Summary:	ECDSA cryptographic signature library
License:	MIT
URL:		http://github.com/tlsfuzzer/python-ecdsa
Source0:	%{pypi_source ecdsa}
BuildArch:	noarch

Requires:	python3-six
Requires:	python3-gmpy
Requires:	python3-gmpy2

%description
This is an easy-to-use implementation of ECDSA cryptography (Elliptic Curve
Digital Signature Algorithm), implemented purely in Python, released under 
the MIT license. With this library, you can quickly create keypairs (signing
 key and verifying key), sign messages, and verify the signatures. The keys 
and signatures are very short, making them easy to handle and incorporate 
into other protocols.

%package -n python3-ecdsa
Summary:	ECDSA cryptographic signature library (pure python)
Provides:	python-ecdsa
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
%description -n python3-ecdsa
This is an easy-to-use implementation of ECDSA cryptography (Elliptic Curve
Digital Signature Algorithm), implemented purely in Python, released under
the MIT license. With this library, you can quickly create keypairs (signing  key and verifying key), sign messages, and verify the signatures. The keys
and signatures are very short, making them easy to handle and incorporate
into other protocols.





%package help
Summary:	Development documents and examples for ecdsa
Provides:	python3-ecdsa-doc
%description help
This is an easy-to-use implementation of ECDSA cryptography (Elliptic Curve
Digital Signature Algorithm), implemented purely in Python, released under
the MIT license. With this library, you can quickly create keypairs (signing  key and verifying key), sign messages, and verify the signatures. The keys
and signatures are very short, making them easy to handle and incorporate
into other protocols.

%prep
%autosetup -n ecdsa-%{version}

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-ecdsa -f filelist.lst
%dir %{python3_sitelib}/*
%exclude %{python3_sitelib}/ecdsa/test_*
%exclude %{python3_sitelib}/ecdsa/__pycache__/test_*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Tue Sep 03 2024 Ge Wang <wang__ge@126.com> - 0.19.0-1
- update to version 0.19.0

* Thu Feb 01 2024 shixuantong <shixuantong1@huawei.com> - 0.18.0-2
- Do not pack test case files

* Thu Aug 04 2022 liqiuyu <liqiuyu@kylinos.cn> - 0.18.0-1
- update version to 0.18.0

* Sat Nov 13 2021 liudabo <liudabo1@huawei.com> - 0.17.0-1
- update version to 0.17.0

* Thu Oct 29 2020 wangye <wangye70@huawei.com> - 0.14.1-3
- use python3 replace python2 for build

* Tue Jun 16 2020 hanhui <hanhui@huawei.com> - 0.14.1-2
- fix python3.8 can not find path

* Tue Feb 11 2020 huzunhao<huzunhao2@huawei.com> - 0.14.1-1
- Package init
